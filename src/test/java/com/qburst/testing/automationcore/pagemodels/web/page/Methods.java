package com.qburst.testing.automationcore.pagemodels.web.page;

import com.qburst.testing.automationcore.selenium.ParentDriver;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Methods {
    public Methods(ParentDriver driver) {

    }
    public static int getHttpStatus(String url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setRequestMethod("HEAD"); // Use HTTP HEAD method to check status only
        connection.connect();

        int statusCode = connection.getResponseCode();
        connection.disconnect();

        return statusCode;
    }

}
