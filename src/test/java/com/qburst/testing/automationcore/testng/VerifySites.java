package com.qburst.testing.automationcore.testng;

import com.qburst.testing.automationcore.pagemodels.web.page.Methods;
import com.qburst.testing.automationcore.pagemodels.web.page.objects.WebPage;
import com.qburst.testing.automationcore.utils.TestLog;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

public class VerifySites extends BaseTest{

     @Test(testName = "Verify Redirects", description = "Verify the status codes of unbranded sites")
     public void verifyRedirect() throws IOException {

         String filePath = "src/test/resources/testconfig.properties";

         Properties properties = new Properties();

         FileInputStream fileInput = new FileInputStream(filePath);
         properties.load(fileInput);
         fileInput.close();

         int linkCount = 1;


         while (true) {
             String linkKey = "link" + linkCount;
             String link = properties.getProperty(linkKey);

             if (link == null) {
                 break; // Exit the loop when there are no more links
             } else {
                 try {
                     int statusCode = Methods.getHttpStatus(link);
                     System.out.println("URL: " + link);
                     System.out.println("Status Code: " + statusCode);
                 } catch (IOException e) {
                     System.err.println("Error checking URL: " + link);
                     e.printStackTrace();
                 }

                 linkCount++;
             }
         }


     }







}


